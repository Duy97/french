//
//  PhraseModel.swift
//  French
//
//  Created by dovietduy on 11/12/20.
//

import Foundation
class PhraseModel {
    var english: String = ""
    var vietnam: String = ""
    var french: String = ""
    var chinese: String = ""
    var favorite: Int = 0
    var voice: String = ""
    var id: Int = 0
    
    init(id: Int, english: String, vietnam: String, french: String, chinese: String, favorite: Int, voice: String) {
        self.english = english
        self.vietnam = vietnam
        self.french = french
        self.chinese = chinese
        self.favorite = favorite
        self.voice = voice
        self.id = id
    }
    init(){
        
    }
}

extension PhraseModel {
    func locaziation() -> String {
        switch LanguageEntity.shared.languageDefaulCode() {
        case "en":
            return english
        case "vi":
            return vietnam
        case "zh-Hant":
            if chinese.contains("@") {
                let strings = chinese.split(separator: "@")
                return String(strings[0])
            }
            return chinese
        default:
            return ""
        }
    }
}
