//
//  AlphabetController.swift
//  French
//
//  Created by dovietduy on 11/8/20.
//

import UIKit
import GoogleMobileAds

class AlphabetController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AudioPlayerDelegate {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblAlphabet: UILabel!
    var listAlphabet = AlphabetEntity.shared.getData()
    var audioPlayer: AudioPlayer!
    let scale = UIScreen.main.bounds.height / 896
    var admobNativeAds: GADUnifiedNativeAd?
    override func viewDidLoad() {
        super.viewDidLoad()
        lblAlphabet.font = lblAlphabet.font?.withSize(16 * scale)
        lblAlphabet.text = "Alphabet".localized()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "AlphabetCell", bundle: nil), forCellWithReuseIdentifier: "AlphabetCell")
        collectionView.register(UINib(nibName: nativeAdmobCLVCell.className, bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: nativeAdmobCLVCell.className)
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadAllNativeAds()
        }
        
        audioPlayer = AudioPlayer()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if PaymentManager.shared.isPurchase(){
            
        } else{
            if let native = AdmobManager.shared.randoomNativeAds(){
                admobNativeAds = native as? GADUnifiedNativeAd
                collectionView.reloadData()
            }
        }
    }
    @IBAction func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        audioPlayer.stop()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listAlphabet.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlphabetCell", for: indexPath) as! AlphabetCell
        let item = listAlphabet[indexPath.row]
        cell.imgIcon.image = UIImage(named: item.icon)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collWidth = collectionView.bounds.width - 20
        
        let deviceType = UIDevice.current.userInterfaceIdiom
        if deviceType == .pad{
            return CGSize(width: Int(collWidth/6), height: Int(collWidth/6))
        }else {
            return CGSize(width: Int(collWidth/3), height: Int(collWidth/3))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // play sound
        let item = listAlphabet[indexPath.row]
        audioPlayer.playAudio(name: item.voice, withExtension: "mp3")
    }
    func didFinishPlaying() {
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader{
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: nativeAdmobCLVCell.className, for: indexPath) as! nativeAdmobCLVCell
            if let native = self.admobNativeAds {
                headerView.setupHeader(nativeAd: native)
            }
            return headerView
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if admobNativeAds == nil{
            return CGSize(width: DEVICE_WIDTH, height: 0)
        }
        return CGSize(width: DEVICE_WIDTH, height: 150 * scale)
    }
}
