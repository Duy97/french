//
//  HomePageController.swift
//  French
//
//  Created by NguyenHuySONCode on 11/5/20.
//
import SideMenu
import UIKit
import StoreKit
import GoogleMobileAds
import UserNotifications

class HomePageController: UIViewController, UITableViewDelegate, UITableViewDataSource, UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var imgAvatar: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblChooseTopic: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var viewSearchContainer: UIView!
    @IBOutlet weak var lblBonjour: UILabel!
    //Ads
    
    var admobNativeAds: GADUnifiedNativeAd?
    var randomCate: CategoryModel!
    var randomPhrase: PhraseModel!
    @IBOutlet weak var topViewSearch: NSLayoutConstraint!
    @IBOutlet weak var topChooseTopic: NSLayoutConstraint!
    @IBOutlet weak var heightLblQuestion: NSLayoutConstraint!
    var audioPlayer: AudioPlayer!
    
    let scale = UIScreen.main.bounds.height / 896
    var listCategory = CategoryEntity.shared.getData()
    var filteredData: [PhraseModel] = []
    var isSearching = false
    var filterString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        config()
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UINib(nibName: "HomePageCell", bundle: nil), forCellReuseIdentifier: "HomePageCell")
        tblView.register(UINib(nibName: "nativeAdmobTBLCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "nativeAdmobTBLCell")
        tblView.register(UINib(nibName: "GreetingsCell", bundle: nil), forCellReuseIdentifier: "GreetingsCell")
        tblView.estimatedRowHeight = 106 * scale
        tblView.rowHeight = UITableView.automaticDimension
        txtSearch.delegate = self
        txtSearch.placeholder = "Search".localized()
        txtSearch.addTarget(self, action: #selector(textFieldDidChange(_:)),
                            for: .editingChanged)
        audioPlayer = AudioPlayer()
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadAllNativeAds()
            AdmobManager.shared.loadBannerView(inVC: self)
        }
        
    }
    func requestAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
            (granted, error) in
            if granted {
                print("yes")
            } else {
                print("No")
            }
        }
    }

    func sendNotification() {
        let content = UNMutableNotificationContent()
        let random = randomData()
        content.title = random.locaziation()
        content.body = random.french
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "greeting_1_m.mp3"))
        if let path = Bundle.main.path(forResource: "AppIcon", ofType: "png") {
            let url = URL(fileURLWithPath: path)

            do {
                let attachment = try UNNotificationAttachment (identifier: "AppIcon", url: url, options: nil)
                content.attachments = [attachment]
            } catch {
                print("The attachment was not loaded.")
            }
        }
        // 3
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1800, repeats: true)

        let request = UNNotificationRequest(identifier: "testNotification", content: content, trigger: trigger)

        // 4
        UNUserNotificationCenter.current().add(request){ error in
            print(error.debugDescription)
        }
    }

    var idRandomPhrase = 0
    func randomData() -> PhraseModel{
        let randomCatgory = CategoryEntity.shared.getData().shuffled()[0]
        self.randomCate = randomCatgory
        let listPhrase = PhraseEntity.shared.getData(item: randomCatgory)
        if !listPhrase.isEmpty{
            let firstId = listPhrase[0].id
            let randomPhrase = listPhrase.shuffled()[0]
            idRandomPhrase = randomPhrase.id - firstId
            self.randomPhrase = randomPhrase
            return randomPhrase
        } else{
            return PhraseModel()
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.identifier == "testNotification" {
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: GreetingsController.className) as! GreetingsController
                viewController.category = self.randomCate
                viewController.selectPhare = IndexPath(row: self.idRandomPhrase, section: 0)
                
                
                let navController = UINavigationController.init(rootViewController: viewController)
                
                if let rootViewController = UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.rootViewController {
                    var currentController = rootViewController
                    while let presentedController = currentController.presentedViewController {
                        currentController = presentedController
                    }
                    navController.isNavigationBarHidden = true
                    navController.modalPresentationStyle = .fullScreen
                    currentController.present(navController, animated: true, completion: nil)
                }
                
            }
            
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if #available(iOS 10.3, *) {
//            SKStoreReviewController.requestReview()
//        } else {
//            
//        }
        if PaymentManager.shared.isPurchase(){
            
        }else{
            if let native = AdmobManager.shared.randoomNativeAds(){
                admobNativeAds = native as? GADUnifiedNativeAd
                tblView.reloadData()
            }
        }
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .notDetermined:
                self.requestAuthorization()
            case .authorized, .provisional:
                self.sendNotification()
            default:
                break // Do nothing
            }
        }
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tblView.reloadData()
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        filteredData = []
        if textField.text == "" {
            isSearching = false
        } else {
            isSearching = true
            filterString = textField.text!
            for item in PhraseEntity.shared.getAllData() {
                if item.locaziation().lowercased().contains((textField.text!.lowercased())){
                    filteredData.append(item)
                }
            }
        }
        self.tblView.reloadData()
    }
    func config() {
        
        if #available(iOS 11.0, *) {
            viewSearchContainer.layer.cornerRadius = 50 * scale
            viewSearchContainer.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            viewSearchContainer.round(corners: [.topLeft, .topRight], cornerRadius: Double(50 * scale))
        }
        viewSearch.layer.cornerRadius = 18 * scale
        topViewSearch.constant = 54 * scale
        topChooseTopic.constant = 40 * scale
        txtSearch.font = txtSearch.font?.withSize(16 * scale)
        lblChooseTopic.font = lblChooseTopic.font?.withSize(16 * scale)
        lblChooseTopic.text = "Choose your topic".localized()
        lblQuestion.text = "What do you want to learn today?".localized()
        lblQuestion.font = lblQuestion.font.withSize(26 * scale)
        lblBonjour.font = lblQuestion.font.withSize(26 * scale)
        heightLblQuestion.constant = 80 * scale
    }
    @IBAction func didSelectBtnSlideMenu(_ sender: Any) {
        
        let menu = storyboard!.instantiateViewController(withIdentifier: "RightMenu") as! SideMenuNavigationController
        menu.menuWidth = 310 * scale
        menu.navigationBar.isHidden = true
        menu.presentationStyle = .menuDissolveIn
        present(menu, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredData.count
        } else {
            return listCategory.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSearching{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GreetingsCell", for: indexPath) as! GreetingsCell
            let item = filteredData[indexPath.row]
            cell.delegate = self
            cell.initData(item: item)
            cell.lblTitle.attributedText = item.locaziation().highlightWordIn(filterString)
            cell.indexPath = indexPath
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomePageCell", for: indexPath) as! HomePageCell
            let item = listCategory[indexPath.row]
            cell.lblTitle.text = item.locaziation()
            cell.lblNumber.text = String(PhraseEntity.shared.count(byCategory: item))
            cell.imgIcon.image = UIImage(named: item.image)
            
            if indexPath.row % 2 != 0 {
                cell.btnStartNow.backgroundColor = #colorLiteral(red: 1, green: 0.862745098, blue: 0.4549019608, alpha: 1)
                cell.btnStartNow.setTitleColor(#colorLiteral(red: 1, green: 0.6588235294, blue: 0.3215686275, alpha: 1), for: .normal)
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isSearching{
            return UITableView.automaticDimension
        } else {
            return 160 * scale
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isSearching{
            audioPlayer.playAudio(name: filteredData[indexPath.row].voice + "_m", withExtension: "mp3")
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "GreetingsCell", for: indexPath) as? GreetingsCell else {
                return
            }
            cell.imgSeen.image = UIImage(named: "ic_seen")
        } else {
            switch listCategory[indexPath.row].id {
            case 0:
                let alphabetVC = storyboard?.instantiateViewController(withIdentifier: AlphabetController.className) as! AlphabetController
                alphabetVC.modalPresentationStyle = .fullScreen
                present(alphabetVC, animated: true, completion: nil)
            default:
                let greetingsVC = storyboard?.instantiateViewController(withIdentifier: GreetingsController.className) as! GreetingsController
                greetingsVC.modalPresentationStyle = .fullScreen
                greetingsVC.category = listCategory[indexPath.row]
                present(greetingsVC, animated: true, completion: nil)
            }
        }
        
    }
}
extension HomePageController{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "nativeAdmobTBLCell") as! nativeAdmobTBLCell
        if let native = self.admobNativeAds {
            headerView.setupHeader(nativeAd: native)
        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if admobNativeAds == nil{
            return 0
        }
        return 160 * scale
    }
}
extension HomePageController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.endEditing(true)
        return true
    }
}
extension HomePageController: GreetingsCellDelegate {
    func didSelectBtnMenuCell(item: PhraseModel) {
        
    }
    
    func didSelectBtnRecord(item: PhraseModel, indexPath: IndexPath) {
        
    }
    
    func didSelectBtnPlayYourVoice(item: PhraseModel) {
        
    }
    
    
}
