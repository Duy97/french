//
//  HomePageCell.swift
//  French
//
//  Created by dovietduy on 11/9/20.
//

import UIKit

class HomePageCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblLearningSet: UILabel!
    @IBOutlet weak var btnStartNow: UIButton!
    
    @IBOutlet weak var bottomBtnStartNow: NSLayoutConstraint!
    @IBOutlet weak var leadingTitle: NSLayoutConstraint!
    
    
    let scale = UIScreen.main.bounds.height / 896
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.cornerRadius = 24 * scale
        btnStartNow.layer.cornerRadius = 15 * scale
        btnStartNow.setTitle("Start Now".localized(), for: .normal)
        btnStartNow.titleLabel?.font = lblLearningSet.font.withSize(12 * scale)
        bottomBtnStartNow.constant = 23 * scale
        leadingTitle.constant = 22 * scale
        lblTitle.font = lblTitle.font.withSize(20 * scale)
        lblNumber.font = lblTitle.font.withSize(10 * scale)
        lblLearningSet.font = lblTitle.font.withSize(10 * scale)
        lblLearningSet.text = "Learning sets".localized()
    }
}
