//
//  TaskController.swift
//  French
//
//  Created by dovietduy on 11/11/20.
//

import UIKit

class TaskController: UIViewController{
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTextQuiz: UILabel!
    @IBOutlet weak var imgTextQuiz: UIImageView!
    @IBOutlet weak var imgAudioQuiz: UIImageView!
    @IBOutlet weak var btnPlayAudio: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var leadiProgressBar: NSLayoutConstraint!
    @IBOutlet weak var topTblView: NSLayoutConstraint!
    @IBOutlet weak var topViewBtnNext: NSLayoutConstraint!
    @IBOutlet weak var topSeparator: NSLayoutConstraint!
    @IBOutlet weak var bottomBtnNext: NSLayoutConstraint!
    @IBOutlet weak var topLblTextQuiz: NSLayoutConstraint!
    @IBOutlet weak var topImgAudioQuiz: NSLayoutConstraint!
    var isTextQuiz = true
    let scale = UIScreen.main.bounds.height / 896
    var listData: [PhraseModel] = []
    var list4Choices: [PhraseModel] = []
    var correctAnswer: PhraseModel!
    var idNext = 0
    var idCorrectAnwser = 0
    var score = 0
    var audioPlayer: AudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.setTitle("Next".localized(), for: .normal)
        btnNext.titleLabel?.font = lblTextQuiz.font.withSize(20 * scale)
        btnNext.layer.cornerRadius = 25 * scale
        lblTextQuiz.font = lblTextQuiz.font?.withSize(26 * scale)
        if #available(iOS 11.0, *) {
            tblView.layer.cornerRadius = 50 * scale
            tblView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            tblView.round(corners: [.topLeft, .topRight], cornerRadius: Double(50 * scale))
        }
        topTblView.constant = 370 * scale
        topViewBtnNext.constant = 705 * scale
        topSeparator.constant = 45 * scale
        bottomBtnNext.constant = 50 * scale
        topLblTextQuiz.constant = 57 * scale
        topImgAudioQuiz.constant = 7 * scale
        leadiProgressBar.constant = 30 * scale
        tblView.delegate = self
        tblView.dataSource = self
        tblView.isScrollEnabled = false
        tblView.register(UINib(nibName: "HeaderTaskCell", bundle: nil), forCellReuseIdentifier: "HeaderTaskCell")
        tblView.register(UINib(nibName: "FooterTaskCell", bundle: nil), forCellReuseIdentifier: "FooterTaskCell")
        tblView.register(UINib(nibName: "TaskCell", bundle: nil), forCellReuseIdentifier: "TaskCell")
        audioPlayer = AudioPlayer()
        audioPlayer.delegate = self
        progressBar.progress = 0.0
        checkTextOrAudioQuiz()
        setupQuestion()
    }
    func checkTextOrAudioQuiz(){
        if isTextQuiz {
            lblTextQuiz.isHidden = false
            imgTextQuiz.isHidden = false
        } else {
            imgAudioQuiz.isHidden = false
            btnPlayAudio.isHidden = false
        }
    }
    func setupQuestion(){
        listData = listData.shuffled()
        correctAnswer = listData[idNext]
        let removeItem = listData.remove(at: idNext)
        list4Choices = []
        for item in listData[0...3]{
            list4Choices.append(item)
        }
        idCorrectAnwser = (0...3).randomElement()!
        list4Choices.insert(removeItem, at: idCorrectAnwser)
        listData.insert(removeItem, at: idNext)
        
        lblTextQuiz.text = list4Choices[idCorrectAnwser].locaziation()
        if !isTextQuiz{
            audioPlayer.playAudio(name: list4Choices[idCorrectAnwser].voice + "_m", withExtension: "mp3")
        }
    }
    @IBAction func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func didSelectBtnNext(_ sender: Any) {
        idNext += 1
        if idNext < listData.count {
            setupQuestion()
            tblView.reloadData()
        } else {
            let alert = UIAlertController(title: "Congratulations".localized(), message: "Thank for participating the game".localized(), preferredStyle: UIAlertController.Style.alert)
            let alertActionOK = UIAlertAction(title: "OK".localized(), style: .default) { (act) in
                self.dismiss(animated: true, completion: nil)
            }
            alert.addAction(alertActionOK)
            self.present(alert, animated: true, completion: nil)
        }
        progressBar.progress += 1 / Float(listData.count)
        progressBar.setProgress(progressBar.progress, animated: true)
    }
    @IBAction func didSelectBtnPlayAudio(_ sender: Any) {
        audioPlayer.playAudio(name: list4Choices[idCorrectAnwser].voice + "_m", withExtension: "mp3")
    }
    
}

extension TaskController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TaskCell
        cell.lblAnswer.text = list4Choices[indexPath.row].french
        cell.viewAnswer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.lblAnswer.textColor = #colorLiteral(red: 0.2666666667, green: 0.2588235294, blue: 0.3843137255, alpha: 1)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65 * scale
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTaskCell") as! HeaderTaskCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 75 * scale
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! TaskCell
        let cellCorrect = tableView.cellForRow(at: IndexPath(row: idCorrectAnwser, section: 0)) as! TaskCell
        if indexPath.row == idCorrectAnwser {
            cell.viewAnswer.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.5529411765, blue: 0.2078431373, alpha: 1)
            cell.lblAnswer.textColor = .white
            score += 1
        } else {
            cell.viewAnswer.backgroundColor = #colorLiteral(red: 0.8823529412, green: 0.1490196078, blue: 0.2117647059, alpha: 1)
            cellCorrect.viewAnswer.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0.5529411765, blue: 0.2078431373, alpha: 1)
            cell.lblAnswer.textColor = .white
            cellCorrect.lblAnswer.textColor = .white
        }
        
    }
}
extension TaskController: AudioPlayerDelegate {
    func didFinishPlaying() {
    }
}
