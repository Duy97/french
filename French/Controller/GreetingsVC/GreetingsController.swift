//
//  GreetingsController.swift
//  French
//
//  Created by dovietduy on 11/9/20.
//

import UIKit
import AVFoundation
import GoogleMobileAds
let PAGE_GREETING = 0
let PAGE_VOCABULARY = 1
let PAGE_FAVORITE = 2

class GreetingsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblGreetings: UILabel!
    @IBOutlet weak var txfSearch: UITextField!
    
    let scale = UIScreen.main.bounds.height / 896
    var category:  CategoryModel!
    var listData: [PhraseModel] = []
    var filteredData: [PhraseModel] = []
    var PAGE_IS = PAGE_GREETING
    var soundRecorder: AVAudioRecorder!
    var recordingSession: AVAudioSession!
    var textToSpeech: TextToSpeech!
    var isRecording = false
    var indexPathRecording: IndexPath!
    var filterString = ""
    var audioPlayer: AudioPlayer!
    var isPlayingAll = false
    //Ads
    var admobNativeAds: GADUnifiedNativeAd?
    var selectPhare = IndexPath(row: 0, section: 0)
    override func viewDidLoad() {
        super.viewDidLoad()
        lblGreetings.font = lblGreetings.font.withSize(16 * scale)
        txfSearch.font = txfSearch.font?.withSize(16 * scale)
        txfSearch.placeholder = "Search".localized()
        if #available(iOS 11.0, *) {
            tblView.layer.cornerRadius = 50 * scale
            tblView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            tblView.round(corners: [.topLeft, .topRight], cornerRadius: Double(50 * scale))
        }
        tblView.delegate = self
        tblView.dataSource = self
        txfSearch.delegate = self
        txfSearch.addTarget(self, action: #selector(textFieldDidChange(_:)),
                                  for: .editingChanged)
        textToSpeech = TextToSpeech()
        textToSpeech.delegate = self
        audioPlayer = AudioPlayer()
        audioPlayer.delegate = self
        recordingSession = AVAudioSession()
        do{
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.overrideOutputAudioPort(.speaker)
            try recordingSession.setActive(true)
        } catch {
            print(error)
        }
        tblView.register(UINib(nibName: "GreetingsCell", bundle: nil), forCellReuseIdentifier: "GreetingsCell")
        tblView.register(UINib(nibName: "nativeAdmobTBLCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "nativeAdmobTBLCell")
        tblView.estimatedRowHeight = 106 * scale
        tblView.rowHeight = UITableView.automaticDimension
        switch PAGE_IS {
        case PAGE_GREETING:
            listData = PhraseEntity.shared.getData(item: category)
            lblGreetings.text = category.locaziation()
        case PAGE_VOCABULARY:
            listData = VocabEntity.shared.getAllData()
            lblGreetings.text = "Vocabulary".localized()
        case PAGE_FAVORITE:
            listData = PhraseEntity.shared.getFavoriteData() + VocabEntity.shared.getFavoriteData()
            lblGreetings.text = "Favorite".localized()
            btnMenu.isHidden = true
        default:
            print("not found")
        }
        filteredData = listData
        if PaymentManager.shared.isPurchase(){
            
        }else{
            AdmobManager.shared.loadAllNativeAds()
            AdmobManager.shared.loadBannerView(inVC: self)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

       tblView.scrollToRow(at: selectPhare, at: .top, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        if PaymentManager.shared.isPurchase(){
            
        }else{
            if let native = AdmobManager.shared.randoomNativeAds(){
                admobNativeAds = native as? GADUnifiedNativeAd
                tblView.reloadData()
            }
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        filteredData = []
        if textField.text == "" {
            filteredData = self.listData
        } else {
            filterString = textField.text!
            for item in listData {
                if item.locaziation().lowercased().contains((textField.text!.lowercased())){
                    filteredData.append(item)
                }
            }
        }
        self.tblView.reloadData()
        audioPlayer.stop()
    }
    @IBAction func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        audioPlayer.stop()
        textToSpeech.stop()
        
    }
    @IBAction func didSelectbtnSearch(_ sender: Any) {
        txfSearch.isHidden = false
        lblGreetings.isHidden = true
        btnSearch.isHidden = true
    }
    var index = 0
    let DID_SELECT_PLAYALL = 1
    let DID_SELECT_ROW = 2
    var flag = 0
   
    @IBAction func didSelectBtnMenu(_ sender: Any) {
        let alert = UIAlertController()
//        let alertActionPractice = UIAlertAction(title: "Practive Now".localized(), style: .default) { (act) in
//            print("action practive now")
//        }
        let alertActionAudioQuiz = UIAlertAction(title: "Audio Quiz".localized(), style: .default) { (act) in
            let taskVC = self.storyboard?.instantiateViewController(withIdentifier: TaskController.className) as! TaskController
            taskVC.modalPresentationStyle = .fullScreen
            taskVC.listData = self.listData
            taskVC.isTextQuiz = false
            self.present(taskVC, animated: true, completion: nil)
        }
        let alertActionTextQuiz = UIAlertAction(title: "Text Quiz".localized(), style: .default) { (act) in
            let taskVC = self.storyboard?.instantiateViewController(withIdentifier: TaskController.className) as! TaskController
            taskVC.modalPresentationStyle = .fullScreen
            taskVC.listData = self.listData
            taskVC.isTextQuiz = true
            self.present(taskVC, animated: true, completion: nil)
        }
        let alertActionCancel = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (act) in
            print("action cancel")
        }
//        alert.addAction(alertActionPractice)
        alert.addAction(alertActionAudioQuiz)
        alert.addAction(alertActionTextQuiz)
        alert.addAction(alertActionCancel)
        if let popoverController = alert.popoverPresentationController {
          popoverController.sourceView = self.view
          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
        self.present(alert, animated: true, completion: nil)
        audioPlayer.stop()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GreetingsCell", for: indexPath) as! GreetingsCell
        let item = filteredData[indexPath.row]
        cell.delegate = self
        cell.initData(item: item)
        cell.indexPath = indexPath
        cell.lblTitle.attributedText = item.locaziation().highlightWordIn(filterString)
        cell.PAGE_IS = PAGE_IS
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 30 * scale
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView()
//        view.backgroundColor = .white
//        return view
//    }
    func loadData(){
        tblView.reloadData()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        flag = DID_SELECT_ROW
        guard let cell = cellForRowAt(indexPath: indexPath) else {
            return
        }
        cell.imgSeen.image = UIImage(named: "ic_seen")
        switch PAGE_IS {
        case PAGE_VOCABULARY:
            textToSpeech.play(text: cell.lblFrenchTitle.text ?? "")
        default:
            audioPlayer.delegate = self
            audioPlayer.playAudio(name: filteredData[indexPath.row].voice + "_m", withExtension: "mp3")
        }
    }
}
extension GreetingsController{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "nativeAdmobTBLCell") as! nativeAdmobTBLCell
        if let native = self.admobNativeAds {
            headerView.setupHeader(nativeAd: native)
        }
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if admobNativeAds == nil{
            return 0
        }
        return 160 * scale
    }
}
extension GreetingsController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        textField.endEditing(true)
        return true
    }
}

extension GreetingsController: GreetingsCellDelegate {
    func didSelectBtnRecord(item: PhraseModel, indexPath: IndexPath) {
        setupRecorder(item: item)
        if isRecording {
            soundRecorder.stop()
            soundRecorder = nil
            guard let cell = cellForRowAt(indexPath: self.indexPathRecording) else {
                return
            }
            cell.btnRecord.setBackgroundImage(UIImage(named: "ic_microphone"), for: .normal)
            cell.btnVoice.isHidden = false
        } else {
            soundRecorder.record()
            guard let cell = cellForRowAt(indexPath: indexPath) else {
                return
            }
            cell.btnRecord.setBackgroundImage(UIImage(named: "ic_recording"), for: .normal)
            self.indexPathRecording = indexPath
        }
        isRecording = !isRecording
    }
    
    func didSelectBtnPlayYourVoice(item: PhraseModel) {
        audioPlayer.delegate = self
        audioPlayer.playAudio(fileName: item.voice + ".m4a")
    }
    
    func didSelectBtnMenuCell(item: PhraseModel) {
        let alert = UIAlertController()
//        let alertActionMore = UIAlertAction(title: "More Actions".localized(), style: .default) { (act) in
//            print("More Actions")
//        }
        let alertActionCopy = UIAlertAction(title: "Copy".localized(), style: .default) { (act) in
            let pasteboard = UIPasteboard.general
            pasteboard.string = item.locaziation()
            self.view.showToast(message: "Copied to clipboard".localized())
        }
        let alertActionShare = UIAlertAction(title: "Share".localized(), style: .default) { (act) in
            var items: [Any] = ["\(item.locaziation())\n\n" + "Content".localized() + "\n\n\(item.french)\n"]
            if let linkApp = URL(string: linkShareApp){
                items.append(linkApp)
            }
            let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
            ac.popoverPresentationController?.sourceView = self.view
            ac.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            self.present(ac, animated: true)
        }
        let alertActionCancel = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (act) in
        }
        if let popoverController = alert.popoverPresentationController {
          popoverController.sourceView = self.view
          popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
          popoverController.permittedArrowDirections = []
        }
//        alert.addAction(alertActionMore)
        alert.addAction(alertActionCopy)
        alert.addAction(alertActionShare)
        alert.addAction(alertActionCancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func setupRecorder(item: PhraseModel) {
        if soundRecorder == nil{
            let audioFilename = getDocumentsDirectory().appendingPathComponent(item.voice + ".m4a")
            let recordSetting = [ AVFormatIDKey : kAudioFormatAppleLossless,
                                  AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue,
                                  AVEncoderBitRateKey : 320000,
                                  AVNumberOfChannelsKey : 2,
                                  AVSampleRateKey : 44100.2] as [String : Any]

            do {
                soundRecorder = try AVAudioRecorder(url: audioFilename, settings: recordSetting )
                soundRecorder.delegate = self
                soundRecorder.prepareToRecord()
            } catch {
                print(error)
            }
        } else {}
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}

extension GreetingsController: AudioPlayerDelegate{
    
    func cellForRowAt(indexPath: IndexPath) -> GreetingsCell? {
        guard let cell = tblView.cellForRow(at: indexPath) as? GreetingsCell else {
            return tblView.dequeueReusableCell(withIdentifier: "GreetingsCell", for: indexPath) as? GreetingsCell
        }
        return cell
    }
    
    @IBAction func didSelectPlayAll(_ sender: Any) {
        flag = DID_SELECT_PLAYALL
        if !isPlayingAll {
            tblView.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
            guard let cell = cellForRowAt(indexPath: IndexPath(row: index, section: 0)) else { return }
            cell.imgSeen.image = UIImage(named: "ic_seen")
            
            switch PAGE_IS {
            case PAGE_VOCABULARY:
                textToSpeech.play(text: cell.lblFrenchTitle.text ?? "")
            case PAGE_FAVORITE:
                textToSpeech.play(text: cell.lblFrenchTitle.text ?? "")
            default:
                audioPlayer.delegate = self
                audioPlayer.playAudio(name: filteredData[index].voice + "_m", withExtension: "mp3")
            }
            btnPlay.setBackgroundImage(#imageLiteral(resourceName: "ic_recording"), for: .normal)
        } else {
            textToSpeech.stop()
            audioPlayer.stop()
            btnPlay.setBackgroundImage(#imageLiteral(resourceName: "play"), for: .normal)
        }
        isPlayingAll = !isPlayingAll
    }
    
    func didFinishPlaying() {
        switch flag {
        case DID_SELECT_PLAYALL:
            index += 1
            if index < filteredData.count{
                tblView.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
                audioPlayer.playAudio(name: filteredData[index].voice + "_m", withExtension: "mp3")
                guard let cell = cellForRowAt(indexPath: IndexPath(row: index, section: 0)) else { return }
                cell.imgSeen.image = UIImage(named: "ic_seen")
            } else {
                index = 0
            }
            print("\(filteredData[index].voice) ending....")
        default:
            print()
            btnPlay.setBackgroundImage(#imageLiteral(resourceName: "play"), for: .normal)
        }

    }
}

extension GreetingsController: AVAudioRecorderDelegate{
    
}
extension GreetingsController: TextToSpeechDelegate {
    func didFinish() {
        switch flag {
        case DID_SELECT_PLAYALL:
            index += 1
            if index < filteredData.count{
                tblView.scrollToRow(at: IndexPath(row: index, section: 0), at: .top, animated: true)
                guard let cell = cellForRowAt(indexPath: IndexPath(row: index, section: 0)) else { return }
                cell.imgSeen.image = UIImage(named: "ic_seen")
                textToSpeech.play(text: cell.lblFrenchTitle.text ?? "")
            } else {
                index = 0
            }
            print("\(filteredData[index].french) ending....")
        default:
            print()
            btnPlay.setBackgroundImage(#imageLiteral(resourceName: "play"), for: .normal)
        }
    }
}
