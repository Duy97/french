//
//  AboutAppController.swift
//  French
//
//  Created by dovietduy on 11/30/20.
//

import UIKit

class AboutAppController: UIViewController {
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.layer.cornerRadius = 15
        textView.setContentOffset(.zero, animated: true)
    }
    @IBAction func didSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
