//
//  SettingController.swift
//  French
//
//  Created by NguyenHuySONCode on 11/5/20.
//

import UIKit
import MessageUI
import StoreKit
let linkShareApp = "https://apps.apple.com/us/app/xcode/id497799835?mt=12"

class SettingController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    let scale = UIScreen.main.bounds.height / 896
    let listMenu: [MenuModel] =
        [MenuModel(title: "Language".localized(), image: "ic_language"),
         MenuModel(title: "Delete all recordings".localized(), image: "ic_delete_all_recordings"),
         MenuModel(title: "Send feedback".localized(), image: "ic_send_feedback"),
         MenuModel(title: "Share app".localized(), image: "ic_share_app"),
         MenuModel(title: "Rate_this_app".localized(), image: "ic_rate_this_app"),
         MenuModel(title: "About app".localized(), image: "ic_about_app")
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "Settings".localized()
        lblTitle.font = lblTitle.font.withSize(17 * scale)
        btnBack.layer.cornerRadius = 17 * scale
        if #available(iOS 11.0, *) {
            tblView.layer.cornerRadius = 50 * scale
            tblView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            tblView.round(corners: [.topLeft, .topRight], cornerRadius: Double(50 * scale))
        }
        tblView.dataSource = self
        tblView.delegate = self
        tblView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
    }
    @IBAction func disSelectBtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        let homePageVC = storyboard?.instantiateViewController(withIdentifier: HomePageController.className) as! HomePageController
        UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = homePageVC
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        let item = listMenu[indexPath.row]
        cell.lblName.text = item.title
        cell.imgIcon.image = UIImage(named: item.image)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68 * scale
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 27 * scale
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: // language
            let languageVC = storyboard?.instantiateViewController(withIdentifier: LanguageController.className) as! LanguageController
            languageVC.modalPresentationStyle = .fullScreen
            present(languageVC, animated: true, completion: nil)
        case 1: // delete all recordings
            view.showToast(message: deleteAllRecordings())
        case 2: // send feedback
            openMailApp()
        case 3: // share app
            if let linkapp = URL(string: linkShareApp){
                let itemsToShare = [linkapp]
                let ac = UIActivityViewController(activityItems: itemsToShare, applicationActivities: nil)
                ac.popoverPresentationController?.sourceView = self.view
                self.present(ac, animated: true)
                print("share oke")
            } else {
                print("Not found app in app store")
            }
        case 4: //rate app
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
            } else {
                if let url = URL(string: linkShareApp) {
                    UIApplication.shared.open(url)
                }
            }
            
        case 5: // about app
            let aboutAppVC = storyboard?.instantiateViewController(withIdentifier: AboutAppController.className) as! AboutAppController
            aboutAppVC.modalPresentationStyle = .fullScreen
            present(aboutAppVC, animated: true, completion: nil)
        default:
            print("i dont know")
        }
    }
    func deleteAllRecordings() -> String {
        let documentsUrl = getDocumentsDirectory()
        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            if fileURLs.count == 0 {
                return "empty"
            }
            for fileURL in fileURLs {
                if fileURL.pathExtension == "m4a" {
                    try FileManager.default.removeItem(at: fileURL)
                }
            }
            return "delete successful"
        } catch  {
            return "delete fail"
            
        }
    }
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func message(_ text : String){
        self.view.showToast(message: text)
    }
    func openMailApp() {
        if MFMailComposeViewController.canSendMail(){
            let composer = MFMailComposeViewController()
            if MFMailComposeViewController.canSendMail() {
                composer.mailComposeDelegate = self
                composer.setToRecipients(["dovietduy97@gmail.com"])
                composer.setSubject("Feedback on learning french app".localized())
                composer.setMessageBody("", isHTML: false)
                present(composer, animated: true, completion: nil)
            }
        }
    }
}
extension SettingController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
        switch result {
        case .sent:
            print("did sent")
        default:
            print("eror sent")
        }
    }
}
