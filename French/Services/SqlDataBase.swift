//
//  SqlDataBase.swift
//  French
//
//  Created by dovietduy on 11/24/20.
//

import Foundation
import SQLite

class SqlDataBase{
    static let shared = SqlDataBase()
    public let connection: Connection?
    
    private init(){
        let dbURL = Bundle.main.url(forResource: "learnfrench", withExtension: "db")!
        
        var newURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        newURL.appendPathComponent("learnfrench.db")
        do {
            if FileManager.default.fileExists(atPath: newURL.path) {
                
            }else {
                try FileManager.default.copyItem(atPath: dbURL.path, toPath: newURL.path)
            }
            print(newURL.path)
        } catch {
            print(error.localizedDescription)
        }
        do{
            connection = try Connection(newURL.path)
        } catch{
            connection = nil
            let nserr = error as NSError
            print("Cannot connect to Database. Error is: \(nserr), \(nserr.userInfo)")
        }
    }
}
