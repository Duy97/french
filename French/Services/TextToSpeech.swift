//
//  TextToSpeech.swift
//  French
//
//  Created by dovietduy on 11/26/20.
//

import Foundation
import AVFoundation

class TextToSpeech: NSObject, AVSpeechSynthesizerDelegate {
    //static let shared = TextToSpeech()
    var synth: AVSpeechSynthesizer!
    var myUtterance: AVSpeechUtterance!
    var delegate: TextToSpeechDelegate!
    override init() {
        
    }
    func play(text: String) {
        myUtterance = AVSpeechUtterance(string: "")
        myUtterance = AVSpeechUtterance(string: text)
        myUtterance.voice = AVSpeechSynthesisVoice(language: "fr-FR")
        myUtterance.rate = 0.5
        synth = AVSpeechSynthesizer()
        synth.delegate = self
        synth.speak(myUtterance)
        print(myUtterance.speechString)
    }
    func playSlowMotion(text: String){
        myUtterance = AVSpeechUtterance(string: text)
        myUtterance.voice = AVSpeechSynthesisVoice(language: "fr-FR")
        myUtterance.rate = 0.0
        synth = AVSpeechSynthesizer()
        synth.delegate = self
        synth.speak(myUtterance)
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        synth.stopSpeaking(at: .immediate)
        delegate?.didFinish()
    }
    func stop(){
        if synth != nil {
            synth = nil
        }
    }
}

protocol TextToSpeechDelegate: class {
    func didFinish()
}
